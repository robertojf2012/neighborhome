package com.example.robert.neighborhome

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_casas.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class CasasA : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_casas)

        val userData = JSONObject(intent.extras.getString("userData"))
        val idFrac = userData.getString("idFrac")

        listCasas.layoutManager = LinearLayoutManager(this)
        getCasas(idFrac)
    }

    fun getCasas(idFrac:String){

        var url = "https://neighborhomeapi.herokuapp.com/api/fracmobile/"+idFrac
        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()

        client.newCall(request).enqueue(object: Callback {

            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {

                when(response.code()){
                    200 -> {
                        var body = response?.body()?.string()

                        this@CasasA.runOnUiThread {

                            var fracData = Gson().fromJson(body,FracResult::class.java) //getting all frac data and converting it to json

                            var array = Gson().toJson(fracData.frac.casas) //getting only array of casas

                            var casas = "{\"casas\":"+array.toString()+"}" //formatting to match CasaResult

                            var casasData = Gson().fromJson(casas,CasaResult::class.java) //mapping data to CasaResult

                            listCasas.adapter = CasaAdapter(casasData,this@CasasA) //send it to adapter to show data
                        }

                    }
                    else -> {
                        this@CasasA.runOnUiThread {
                            Toast.makeText(applicationContext,"Error de servidor ${response.code()}", Toast.LENGTH_SHORT).show()
                            Toast.makeText(applicationContext,response.message(), Toast.LENGTH_SHORT).show()
                        }
                    }
                }

            }

        })

    }
}
