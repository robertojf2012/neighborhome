package com.example.robert.neighborhome
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.login_layout.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import okhttp3.RequestBody


class LoginA : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_layout)

        var btnLogin = findViewById<Button>(R.id.btnLogin)

        btnLogin.setOnClickListener {

            var email = etEmail.text.toString()
            var password = etPassword.text.toString()
            login(email,password)

        }
    }

    fun login(email:String, password:String){

        val url = "https://neighborhomeapi.herokuapp.com/api/login"
        val JSON = MediaType.parse("application/json; charset=utf-8")

        var data = JSONObject()

        data.put("email",email)
        data.put("password",password)

        val body = RequestBody.create(JSON, data.toString())

        val request = Request.Builder().url(url).post(body).build()

        val client = OkHttpClient()

        client.newCall(request).enqueue(object: Callback{

            override fun onFailure(call: Call, e: IOException) {
                this@LoginA.runOnUiThread {
                    Toast.makeText(applicationContext,"Error de peticion",Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call, response: Response) {

                when(response.code()){
                    200 -> {
                        var body = response?.body()?.string()
                        val intent = Intent(this@LoginA, HomeA::class.java)
                        intent.putExtra("userData",body) //saving the json data for next activity
                        startActivity(intent)
                    }
                    404 -> {
                        this@LoginA.runOnUiThread {
                            Toast.makeText(applicationContext,"Usuario o contrasena incorrecto",Toast.LENGTH_SHORT).show()
                        }
                    }
                    else -> {
                        this@LoginA.runOnUiThread {
                            Toast.makeText(applicationContext,"Error de servidor ${response.code()}",Toast.LENGTH_SHORT).show()
                            Toast.makeText(applicationContext,response.message(),Toast.LENGTH_SHORT).show()
                        }
                    }
                }

            }

        })
    }


}
