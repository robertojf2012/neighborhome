package com.example.robert.neighborhome

data class FracResult(val frac:Fraccionamiento) // val frac es la clave que tiene el arreglo de JSON

data class CasaResult(val casas: List<Casa>)

data class Fraccionamiento(
    val strNombre: String,
    val strDescripcion: String,
    val strLogo: String,
    val intLatitud: Number,
    val intLongitud: Number,
    val cuotas: List<Cuota>,
    val casas: List<Casa>,
    val noticias: List<Noticia>
)

data class Cuota(
    val intMes:Number,
    val intAno:Number,
    val doubleMonto: Number
)

data class Casa(
    val intNoCasa:Number,
    val strModelo:String,
    val strCalle: String
)

data class Noticia(
    val strTitulo:String,
    val strDescripcion:String,
    val strFecha: String
)




