package com.example.robert.neighborhome

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_home.*
import org.json.JSONObject

class HomeA : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val userData = JSONObject(intent.extras.getString("userData"))

        Log.d("DATA",userData.toString())

        tvWelcome.setText("Bienvenido ${userData.getString("strNombre")} " +
                                     "${userData.getString("strApat")} " +
                                     "${userData.getString("strAmat")}")

        btnNoticias.setOnClickListener {
            val intent = Intent(this, NoticiasA::class.java)
            startActivity(intent)
        }

        btnCasas.setOnClickListener {
            val intent = Intent(this, CasasA::class.java)
            intent.putExtra("userData",userData.toString()) //saving the json data for next activity
            startActivity(intent)
        }

        btnUsuarios.setOnClickListener {
            val intent = Intent(this, UsuariosA::class.java)
            intent.putExtra("userData",userData.toString()) //saving the json data for next activity
            startActivity(intent)
        }

        btnPagos.setOnClickListener {
            val intent = Intent(this, PagosA::class.java)
            startActivity(intent)
        }
    }
}
