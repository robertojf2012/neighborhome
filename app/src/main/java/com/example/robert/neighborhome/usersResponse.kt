package com.example.robert.neighborhome

//Creando el objeto que almacenara toda la lista de usuarios

data class UserResult(val users:List<User>) // val users es la clave que tiene el arreglo de JSON

//Modelo de respuesta JSON
data class User(
    val id: String?,
    val idFrac: String?,
    val intNoCasa: Number?,
    val strEmail: String?,
    val strPassword: String?,
    val strNombre: String?,
    val strApat: String?,
    val strAmat: String?,
    val intTelefono: Number?,
    val boolAdmin: Boolean?
)