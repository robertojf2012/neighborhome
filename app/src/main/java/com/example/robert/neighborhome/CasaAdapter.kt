package com.example.robert.neighborhome

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.casa_item.view.*

class CasaAdapter(private val casas : CasaResult, private val context: Context): RecyclerView.Adapter<CasaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.casa_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = casas.casas.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindRepo(casas.casas[position])
        holder.itemView.setOnClickListener {

        }
    }

    //Esta clase se encarga de llenar la informacion
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindRepo(casa: Casa) {
            itemView.tvNumCasa.text = casa.intNoCasa.toString()
            itemView.tvModelo.text = casa.strModelo
            itemView.tvCalle.text = casa.strCalle
        }
    }
}