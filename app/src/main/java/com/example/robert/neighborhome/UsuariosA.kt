package com.example.robert.neighborhome

import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_usuarios.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import android.Manifest.permission.CALL_PHONE
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat


class UsuariosA : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_usuarios)

        val userData = JSONObject(intent.extras.getString("userData"))
        val idFrac = userData.getString("idFrac")
        //Log.d("IDD",idFrac)


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) { }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE),1)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
            listUsuarios.layoutManager = LinearLayoutManager(this)
            getUsuarios(idFrac)
        }



    }

    fun getUsuarios(idFrac:String){

        var url = "https://neighborhomeapi.herokuapp.com/api/usersmobile/"+idFrac
        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()

        client.newCall(request).enqueue(object: Callback {

            override fun onFailure(call: Call, e: IOException) {
                Log.d("INFOO","ERROR")
                Log.d("INFOO",e.message)
                Log.d("INFOO",e.toString())
            }

            override fun onResponse(call: Call, response: Response) {

                when(response.code()){
                    200 -> {
                        var body = response?.body()?.string()
                        Log.d("INFOO",body)

                        this@UsuariosA.runOnUiThread {
                            var usersData = Gson().fromJson(body,UserResult::class.java)
                            listUsuarios.adapter = UsuarioAdapter(usersData,this@UsuariosA)
                        }

                    }
                    else -> {
                        this@UsuariosA.runOnUiThread {
                            Toast.makeText(applicationContext,"Error de servidor ${response.code()}", Toast.LENGTH_SHORT).show()
                            Toast.makeText(applicationContext,response.message(), Toast.LENGTH_SHORT).show()
                        }
                    }
                }

            }

        })
    }
}
