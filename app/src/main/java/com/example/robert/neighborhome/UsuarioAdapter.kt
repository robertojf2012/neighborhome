package com.example.robert.neighborhome

import android.Manifest
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.usuario_item.view.*
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log


class UsuarioAdapter(private val usuarios : UserResult, private val context: Context): RecyclerView.Adapter<UsuarioAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.usuario_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = usuarios.users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindRepo(usuarios.users[position])
        holder.itemView.setOnClickListener {

        }

        holder.itemView.btnCall.setOnClickListener {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                Log.d("INFOO","PERMISO NO OTORGADO")
            }else{
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:"+usuarios.users[position].intTelefono)
                startActivity(context,callIntent, Bundle())
            }
        }

    }

    //Esta clase se encarga de llenar la informacion
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindRepo(usuario: User) {
            itemView.tvNumCasa.text = usuario.strNombre + ' '+ usuario.strApat + ' ' + usuario.strAmat
            itemView.tvModelo.text = usuario.intNoCasa.toString()
            itemView.tvCalle.text = usuario.strEmail
        }
    }
}